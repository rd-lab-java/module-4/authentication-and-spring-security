package com.epam.esm.api.v1;

import com.epam.esm.api.assembler.OrderResourceAssembler;
import com.epam.esm.entity.order.Order;
import com.epam.esm.entity.order.OrderResource;
import com.epam.esm.entity.user.User;
import com.epam.esm.service.exception.OrderNotFoundException;
import com.epam.esm.service.order.OrderManager;
import com.epam.esm.service.user.UserManager;
import com.epam.esm.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Rest api controller for order
 */
@RestController
@RequestMapping("/v1/orders")
public class OrderController {
    @Autowired
    UserManager userManager;
    @Autowired
    private OrderManager orderManager;
    @Autowired
    private OrderResourceAssembler orderResourceAssembler;

    /**
     * Find order by id
     * @param orderId order id that is to be found
     * @return The common response {@link ResponseUtils} containing status code,
     *  message and actual data ({@link Order})
     * @throws RuntimeException {@link OrderNotFoundException} when no any data based on order id.
     */
    @PreAuthorize(value = "hasRole('ADMIN') or hasPermission(#orderId, 'order', 'ORDER_READ')")
    @GetMapping("/{orderId:[0-9]+}")
    public ResponseUtils findOrderById(@PathVariable long orderId) {
        Order order = orderManager.findById(orderId);
        OrderResource orderResource = orderResourceAssembler.toResource(order);
        return ResponseUtils.success(orderResource);
    }

    /**
     * Delete order by id
     * @param orderId id of an Order that is to be deleted
     * @return The common response {@link ResponseUtils} containing status code,
     *  message and actual data ({@link Order})
     * @throws RuntimeException {@link OrderNotFoundException} when no any data based on order id.
     */
    @PreAuthorize(value = "hasRole('ADMIN') or hasPermission(#orderId, 'order', 'ORDER_WRITE')")
    @DeleteMapping("/{orderId:[0-9]+}")
    public ResponseUtils deleteOrderById(@PathVariable long orderId) {
        Order deletedOrder = orderManager.deleteById(orderId);
        return ResponseUtils.response(200, "Order deleted successfully",
                orderResourceAssembler.toResource(deletedOrder));
    }
}
