package com.epam.esm.api.assembler;

import com.epam.esm.api.v1.TagController;
import com.epam.esm.entity.tag.Tag;
import com.epam.esm.entity.tag.TagResource;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class TagResourceAssembler {
    public TagResource toResource(Tag tag) {
        TagResource resource = new TagResource(tag.getId(), tag.getName());
        resource.add(linkTo(methodOn(TagController.class).findTag(tag.getId())).withSelfRel());
        resource.add(linkTo(methodOn(TagController.class).findGiftCertificatesByTagId(tag.getId(), Pageable.unpaged())).
                withRel("giftCertificates"));
        return resource;
    }
}
