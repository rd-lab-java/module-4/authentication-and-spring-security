package com.epam.esm.api.v1;

import com.epam.esm.api.assembler.*;
import com.epam.esm.api.common.PatchHelper;
import com.epam.esm.entity.account.Account;
import com.epam.esm.entity.giftcertificate.GiftCertificate;
import com.epam.esm.entity.giftcertificate.GiftCertificateListResource;
import com.epam.esm.entity.giftcertificate.GiftCertificateMapper;
import com.epam.esm.entity.giftcertificate.GiftCertificateResourceInput;
import com.epam.esm.entity.order.Order;
import com.epam.esm.entity.order.OrderStatus;
import com.epam.esm.entity.tag.Tag;
import com.epam.esm.entity.tag.TagListResource;
import com.epam.esm.entity.user.User;
import com.epam.esm.service.account.AccountManager;
import com.epam.esm.service.exception.GiftCertificateDuplicateException;
import com.epam.esm.service.exception.GiftCertificateNotFoundException;
import com.epam.esm.service.exception.GiftCertificateNotModifiedException;
import com.epam.esm.service.giftcertificate.GiftCertificateManager;
import com.epam.esm.service.order.OrderManager;
import com.epam.esm.service.tag.TagManager;
import com.epam.esm.service.user.UserManager;
import com.epam.esm.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.json.JsonMergePatch;
import java.security.Principal;
import java.util.Optional;

/**
 * Rest controller api for managing 
 * gift certificate based requests and responses.
 * @author yusuf
 *
 */
@RestController
@RequestMapping("/v1/gift-certificates")
public class GiftCertificateController {
	/** Default order quantity value */
	private static final String DEFAULT_ORDER_QUANTITY= "1";
	/** Gift Certificate Service class variable */
	@Autowired
	private GiftCertificateManager giftCertificateManager;
	/** Tag Service class variable */
	@Autowired
	private TagManager tagManager;
	@Autowired
	private OrderManager orderManager;
	@Autowired
	private UserManager userManager;
	@Autowired
	private AccountManager accountManager;
	@Autowired
	private GiftCertificateMapper giftCertificateMapper;
	@Autowired
	private PatchHelper patchHelper;
	@Autowired
	private GiftCertificateListResourceAssembler giftCertificateListResourceAssembler;
	@Autowired
	private GiftCertificateResourceAssembler giftCertificateResourceAssembler;
	@Autowired
	private OrderListResourceAssembler orderListResourceAssembler;
	@Autowired
	private OrderResourceAssembler orderResourceAssembler;
	@Autowired
	private TagListResourceAssembler tagListResourceAssembler;

	/**
	 * Used to save new gift certificate, can contain zero or more tags
	 * 
	 * @param giftCertificate
	 * @return The common response {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate})
	 * @throws Exception {@link GiftCertificateDuplicateException}
	 *  when trying to insert duplicate gift certificate
	 */
	@PreAuthorize(value = "hasRole('ADMIN') or hasAuthority('GIFTCERTIFICATE_WRITE')")
	@PostMapping(consumes = "application/json", produces = "application/json")
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseUtils saveGiftCertificate(@RequestBody GiftCertificate giftCertificate) {
		GiftCertificate savedGiftCertificate = giftCertificateManager.save(giftCertificate);
		return ResponseUtils.response(201, "Gift certificate created successfully",
				giftCertificateResourceAssembler.toResource(savedGiftCertificate));
	}
	
	/**
	 * Used to find all gift certificates
	 *
	 * @return {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate}) without tags
	 * @throws Exception {@link GiftCertificateNotFoundException}
	 *  when no found any gift certificate
	 */
	@PreAuthorize(value = "hasAnyRole('USER', 'ADMIN') and hasAuthority('GIFTCERTIFICATE_READ')")
	@GetMapping(produces = "application/json")
	public ResponseUtils findAll(Pageable pageable) {
		Page<GiftCertificate> giftCertificatePage = giftCertificateManager.findAll(pageable);
		GiftCertificateListResource giftCertificateListResource = giftCertificateListResourceAssembler.
				build(giftCertificatePage);
		return ResponseUtils.success(giftCertificateListResource);
	}
	
	/**
	 * Used to find gift certificate by gift certificate id
	 *
	 * @param id
	 * @return The common response {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate}) without tags
	 * @throws Exception {@link GiftCertificateNotFoundException}
	 *  when no found any gift certificate based on id
	 */
	@PreAuthorize(value = "hasAnyRole('USER', 'ADMIN') and hasAuthority('GIFTCERTIFICATE_READ')")
	@GetMapping(path = "/{id:[0-9]+}", produces = "application/json")
	public ResponseUtils findGiftCertificateById(@PathVariable long id) {
		GiftCertificate giftCertificate = giftCertificateManager.findById(id);
		return ResponseUtils.success(giftCertificateResourceAssembler.toResource(giftCertificate));
	}
	
	/**
	 * Used to update the only gift certificate's different fields,
	 * or tags that is new to the gift certificate
	 * 
	 * @param id of {@link GiftCertificate} that is to be updated
	 * @param {@link GiftCertificate} contains new values 
	 * @return The common response {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate})
	 * @throws Exception {@link GiftCertificateNotFoundException}
	 * when no found gift certificate based id 
	 * or not modified exception {@link GiftCertificateNotModifiedException}
	 * when try to update unmodified gift certificate
	 */
	@PreAuthorize(value = "hasRole('ADMIN') or hasAuthority('GIFTCERTIFICATE_WRITE')")
	@PutMapping(path = "/{id:[0-9]+}", produces = "application/json", consumes = "application/json")
	public ResponseUtils updateGetCertificate(@PathVariable long id, @RequestBody GiftCertificate giftCertificate) {
		giftCertificate.setId(id);
		GiftCertificate updatedGiftCertificate = giftCertificateManager.update(giftCertificate);
		return ResponseUtils.success(giftCertificateResourceAssembler.toResource(updatedGiftCertificate));
	}
	
	/**
	 * Used to delete gift certificate 
	 * 
	 * @param {@link GiftCertificate} id 
	 * @return The common response {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate})
	 * @throws Exception {@link GiftCertificateNotFoundException}
	 * when no found any gift certificate based on id 
	 */
	@PreAuthorize(value = "hasRole('ADMIN') or hasAuthority('GIFTCERTIFICATE_WRITE')")
	@DeleteMapping(path="/{id:[0-9]+}", produces = "application/json")
	public ResponseUtils deleteGiftCertificate(@PathVariable long id) {
		GiftCertificate deletedGiftCertificate = giftCertificateManager.deleteById(id);
		return ResponseUtils.response(200, "Gift Certificate deleted successfully",
				giftCertificateResourceAssembler.toResource(deletedGiftCertificate));
	}

	/**
	 * Used to find gift certificate tags
	 *
	 * @param {@link GiftCertificate} id
	 * @return The common response {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate})
	 */
	@PreAuthorize(value = "hasAnyRole('USER', 'ADMIN') and hasAuthority('GIFTCERTIFICATE_READ')")
	@GetMapping("/{giftCertId:[0-9]+}/tags")
	public ResponseUtils findGiftCertificateTags(@PathVariable long giftCertId,
												 Pageable pageable) {
		Page<Tag> tagPage = tagManager.findByGiftCertificateId(giftCertId, pageable);
		TagListResource tagListResource = tagListResourceAssembler.build(tagPage);
		return ResponseUtils.success(tagListResource);
	}

	/**
	 * Used to update single field of gift certificate
	 * @param giftCertificateId
	 * @param mergePatch a single value to be updated
	 * @return The common response {@link ResponseUtils} containing status code,
	 * 	 *  message and actual data ({@link GiftCertificate})
	 */
	@PreAuthorize(value = "hasRole('ADMIN') or hasAuthority('GIFTCERTIFICATE_WRITE')")
	@PatchMapping(value = "/{giftCertId:[0-9]+}", consumes = "application/merge-patch+json")
	public ResponseUtils updateGiftCertificateField(@PathVariable("giftCertId") long giftCertificateId,
													@RequestBody JsonMergePatch mergePatch) {
		GiftCertificate giftCertificate = giftCertificateManager.findById(giftCertificateId);
		GiftCertificateResourceInput giftCertificateResource = giftCertificateMapper.
				asGiftCertificateResourceInput(giftCertificate);
		GiftCertificateResourceInput giftCertificateResourcePatched = patchHelper.
				mergePatch(mergePatch, giftCertificateResource, GiftCertificateResourceInput.class);
		giftCertificateMapper.update(giftCertificateResourcePatched, giftCertificate);
		GiftCertificate updatedGiftCertificate = giftCertificateManager.update(giftCertificate);
		return ResponseUtils.success(giftCertificateResourceAssembler.toResource(updatedGiftCertificate));
	}

	/**
	 * Create order by gift certificate id
	 * @param giftCertificateId id of gift certificate order belonging to
	 * @param quantity quantity of order
	 * @return The common response {@link ResponseUtils} containing status code,
	 * 	 * 	 *  message and actual data ({@link Order})
	 */
	@PreAuthorize(value = "hasRole('ADMIN') or hasPermission(#userId, 'order', 'ORDER_WRITE')")
	@PostMapping("/{giftCertificateId:[0-9]+}/orders")
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseUtils orderGiftCertificate(@PathVariable long giftCertificateId, @RequestParam long userId,
											  @RequestParam(required = false, defaultValue = DEFAULT_ORDER_QUANTITY)
														  int quantity) {
		GiftCertificate giftCertificate = giftCertificateManager.findById(giftCertificateId);
		User owner = userManager.findById(userId);
		Order order = new Order(quantity, OrderStatus.UNPAID, owner, giftCertificate);
		Order savedOrder = orderManager.save(order);
		return ResponseUtils.response(201, "Gift certificate order created successfully",
				orderResourceAssembler.toResource(savedOrder));
	}

	/**
	 * Get all orders belonging to the specific gift certificate
	 * @param giftCertificateId id of gift certificate that order belonging to
	 * @param pageable page number and size of the page
	 * @return The common response {@link ResponseUtils} containing status code,
	 * 	 * 	 *  message and actual all data ({@link Order})
	 */
	@PreAuthorize(value = "hasRole('ADMIN') and hasAuthority('ORDER_READ')")
	@GetMapping("/{giftCertificateId:[0-9]+}/orders")
	public ResponseUtils findGiftCertificateOrders(@PathVariable long giftCertificateId, Pageable pageable) {
		Page<Order> giftCertificateOrdersPage = orderManager.findByGiftCertificateId(giftCertificateId, pageable);
		return ResponseUtils.success(orderListResourceAssembler.build(giftCertificateOrdersPage));
	}

	/**
	 * Search gift certificate by tag(s)
	 * @param tags name of tag(s)
	 * @param pageable page number, page size and sort
	 * @return The common response {@link ResponseUtils} containing status code,
	 * 	 * 	 *  message and actual all data ({@link GiftCertificate})
	 */
	@PreAuthorize(value = "hasAnyRole('USER', 'ADMIN') and hasAuthority('GIFTCERTIFICATE_READ')")
	@GetMapping("/search-by-tag")
	public ResponseUtils searchByTag(@RequestParam(value = "tag") Optional<String[]> tags, Pageable pageable) {
		return ResponseUtils.success(giftCertificateListResourceAssembler.
				build(giftCertificateManager.findByTagNames(tags, pageable)));
	}

	/**
	 * Search gift certificates by part of gift certificate's name
	 * @param name	part of a name
	 * @param pageable page number, page size and sort
	 * @return The common response {@link ResponseUtils} containing status code,
	 * 	 * 	 * 	 *  message and actual all data ({@link GiftCertificate})
	 */
	@PreAuthorize(value = "hasAnyRole('USER', 'ADMIN') and hasAuthority('GIFTCERTIFICATE_READ')")
	@GetMapping("/search-by-name")
	public ResponseUtils searchByName(@RequestParam(value = "name") Optional<String> name, Pageable pageable) {
		return ResponseUtils.success(giftCertificateListResourceAssembler.
				build(giftCertificateManager.findByNameContaining(name, pageable)));
	}

	/**
	 * Search gift certificates by part of gift certificate's description
	 * @param description part of description
	 * @param pageable page number, page size and sort
	 * @return The common response {@link ResponseUtils} containing status code,
	 * 	 * 	 * 	 message and actual all data ({@link GiftCertificate})
	 */
	@PreAuthorize(value = "hasAnyRole('USER', 'ADMIN') and hasAuthority('GIFTCERTIFICATE_READ')")
	@GetMapping("/search-by-description")
	public ResponseUtils searchByDescription(@RequestParam(value = "description") Optional<String> description,
											 Pageable pageable) {
		return ResponseUtils.success(giftCertificateListResourceAssembler.
				build(giftCertificateManager.findByDescriptionContaining(description, pageable)));
	}
}
