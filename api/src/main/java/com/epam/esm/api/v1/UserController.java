package com.epam.esm.api.v1;

import com.epam.esm.api.assembler.OrderListResourceAssembler;
import com.epam.esm.api.assembler.UserListResourceAssembler;
import com.epam.esm.api.assembler.UserResourceAssembler;
import com.epam.esm.entity.order.Order;
import com.epam.esm.entity.user.User;
import com.epam.esm.entity.user.UserListResource;
import com.epam.esm.service.exception.UserDuplicateException;
import com.epam.esm.service.exception.UserNotFoundException;
import com.epam.esm.service.order.OrderManager;
import com.epam.esm.service.user.UserManager;
import com.epam.esm.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Rest api controller for user
 */
@RestController
@RequestMapping("/v1/users")
public class UserController {

    @Autowired
    private UserManager userManager;
    @Autowired
    private OrderManager orderManager;
    @Autowired
    private UserListResourceAssembler userListResourceAssembler;
    @Autowired
    private UserResourceAssembler userResourceAssembler;
    @Autowired
    private OrderListResourceAssembler orderListResourceAssembler;

    /**
     * Find all users
     * @param pageable page number, page size and sort values
     * @return The common response {@link ResponseUtils} containing status code,
     * 	 * 	 * 	 *  message and actual all data ({@link User})
     */
    @PreAuthorize(value = "hasRole('ADMIN') and hasAuthority('USER_READ')")
    @GetMapping
    public ResponseUtils findUsers(Pageable pageable) {
        Page<User> userPage = userManager.findAll(pageable);
        UserListResource userListResource = userListResourceAssembler.build(userPage);
        return ResponseUtils.success(userListResource);
    }

    /**
     * Find an user by id
     * @param userId user id value
     * @return The common response {@link ResponseUtils} containing status code,
     * 	 * 	 * 	 *  message and actual data ({@link User})
     * 	 @throws RuntimeException {@link UserNotFoundException} when no any data under the provided id.
     */
    @PreAuthorize(value = "hasRole('ADMIN') and hasPermission(#userId, 'user', 'USER_READ')")
    @GetMapping("/{userId:[0-9]+}")
    public ResponseUtils findUserById(@PathVariable long userId) {
        User user = userManager.findById(userId);
        return ResponseUtils.success(userResourceAssembler.toResource(user));
    }

    /**
     * Save a new user
     * @param user user object that is to be saved
     * @return The common response {@link ResponseUtils} containing status code,
     * 	 * 	 * 	 *  message and actual data ({@link User})
     * @throws RuntimeException {@link UserDuplicateException} when saving user with the same email address.
     */
    @PreAuthorize("hasRole('ADMIN') and hasAuthority('USER_WRITE')")
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseUtils saveUser(@RequestBody User user) {
        User savedUser = userManager.save(user);
        return ResponseUtils.response(201,"User created successfully",
                userResourceAssembler.toResource(savedUser));
    }

    /**
     * Delete user
     * @param userId id value of user to be deleted
     * @return The common response {@link ResponseUtils} containing status code,
     * 	 * 	 * 	 *  message and actual data ({@link User})
     * @throws RuntimeException {@link UserNotFoundException} when no any data under the provided id.
     */
    @PreAuthorize(value = "hasRole('ADMIN') or hasPermission(#userId, 'user', 'USER_WRITE')")
    @DeleteMapping("/{userId:[0-9]+}")
    public ResponseUtils deleteUser(@PathVariable long userId) {
        User deletedUser = userManager.deleteById(userId);
        return ResponseUtils.response(200, "User deleted successfully",
                userResourceAssembler.toResource(deletedUser));
    }

    /**
     * Find all orders belonging to an user
     * @param userId id value of user containing orders
     * @param pageable page number, page size and sort values
     * @return The common response {@link ResponseUtils} containing status code,
     * 	 * 	 * 	 *  message and actual all data ({@link User})
     */
    @PreAuthorize(value = "hasRole('ADMIN') or hasPermission(#userId, 'order', 'ORDER_READ')")
    @GetMapping("/{userId:[0-9]+}/orders")
    public ResponseUtils findOrders(@PathVariable long userId,
                                    Pageable pageable) {
        Page<Order> orderPage = orderManager.findByOwnerId(userId, pageable);
        return ResponseUtils.success(orderListResourceAssembler.build(orderPage));
    }

}
