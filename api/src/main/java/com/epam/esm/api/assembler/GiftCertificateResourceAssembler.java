package com.epam.esm.api.assembler;

import com.epam.esm.api.v1.GiftCertificateController;
import com.epam.esm.entity.giftcertificate.GiftCertificate;
import com.epam.esm.entity.giftcertificate.GiftCertificateResource;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class GiftCertificateResourceAssembler {
    public GiftCertificateResource toResource(GiftCertificate giftCertificate) {
        GiftCertificateResource resource = new GiftCertificateResource(giftCertificate.getId(), giftCertificate.getName(),
                giftCertificate.getDescription(), giftCertificate.getPrice(), giftCertificate.getDuration(),
                giftCertificate.getCreatedDate(), giftCertificate.getUpdatedDate());
        resource.add(linkTo(methodOn(GiftCertificateController.class).findGiftCertificateById(giftCertificate.getId()))
                .withSelfRel());
        resource.add(linkTo(methodOn(GiftCertificateController.class).findGiftCertificateTags(giftCertificate.getId(),
                Pageable.unpaged())).withRel("tags"));
        resource.add(linkTo(methodOn(GiftCertificateController.class).findGiftCertificateOrders(giftCertificate.getId(),
                Pageable.unpaged())).withRel("orders"));
        
        return resource;
    }
}
