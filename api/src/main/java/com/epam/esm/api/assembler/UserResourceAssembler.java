package com.epam.esm.api.assembler;


import com.epam.esm.api.v1.UserController;
import com.epam.esm.entity.user.User;
import com.epam.esm.entity.user.UserResource;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class UserResourceAssembler {
    public UserResource toResource(User user) {
        UserResource userResource = new UserResource(user.getId(), user.getFullName(),
                user.getCreatedDate(), user.getLastUpdatedDate());
        userResource.add(linkTo(methodOn(UserController.class).findUserById(user.getId())).withSelfRel());
        userResource.add(linkTo(methodOn(UserController.class).
                findOrders(user.getId(), Pageable.unpaged())).withRel("orders"));
        return userResource;
    }
}
