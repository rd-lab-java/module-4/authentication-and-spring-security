package com.epam.esm.api.assembler;

import com.epam.esm.api.common.PageLinks;
import com.epam.esm.api.v1.UserController;
import com.epam.esm.entity.user.User;
import com.epam.esm.entity.user.UserListResource;
import com.epam.esm.entity.user.UserResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserListResourceAssembler {
    @Autowired
    private UserResourceAssembler userResourceAssembler;

    @PageLinks(UserController.class)
    public UserListResource build(Page<User> page) {

        List<UserResource> userResources = page.getContent().stream()
                .map(user -> userResourceAssembler.toResource(user))
                .collect(Collectors.toList());

        return new UserListResource(userResources, page.getNumber(), page.getSize(),
                page.getTotalPages(), page.getTotalElements());
    }
}
