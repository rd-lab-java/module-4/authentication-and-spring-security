package com.epam.esm.api.v1;

import com.epam.esm.entity.account.RegisterRequest;
import com.epam.esm.service.account.AccountManager;
import com.epam.esm.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest Controller api for mapping
 * account based requests and responses
 * @author yusuf
 */

@RestController
public class AccountController {
    @Autowired
    private AccountManager accountManager;

    /**
     * Used to register new account with an user role
     * @param registerRequest contains necessary parameters for new account
     * @return The common response {@link ResponseUtils} containing status code,
     *  message and boolean result ({@link Boolean})
     */
    @PostMapping("/v1/register")
    public ResponseUtils saveAccount(@RequestBody RegisterRequest registerRequest) {
        boolean isSaved = accountManager.saveAccountAsUser(registerRequest);
        return isSaved ? ResponseUtils.response(200, "Account registered successfully", true) :
                ResponseUtils.response(500, "Registering account failed", false);
    }
}
