package com.epam.esm.entity.order;


import com.epam.esm.entity.common.AbstractListResource;

import java.util.Collection;

public class OrderListResource extends AbstractListResource {
    private final Collection<OrderResource> orderResources;

    public OrderListResource(Collection<OrderResource> orderResources, int pageNumber, int pageSize,
                             int totalPages, long totalElements) {
        super(pageNumber, pageSize, totalPages, totalElements);
        this.orderResources = orderResources;
    }

    public Collection<OrderResource> getOrderResources() {
        return orderResources;
    }
}
