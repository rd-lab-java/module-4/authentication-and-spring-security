package com.epam.esm.entity.tag;


import com.epam.esm.entity.common.AbstractListResource;

import java.util.Collection;

public class TagListResource extends AbstractListResource {
    private final Collection<TagResource> tags;

    public TagListResource(Collection<TagResource> tags, int pageNumber, int pageSize, int totalPages, long totalElements) {
        super(pageNumber, pageSize, totalPages, totalElements);
        this.tags = tags;
    }

    public Collection<TagResource> getTags() {
        return tags;
    }
}
