package com.epam.esm.entity.giftcertificate;


public interface GiftCertificateFieldMapper {
    String retrieveChangedFields(GiftCertificate original, GiftCertificate another);
}
