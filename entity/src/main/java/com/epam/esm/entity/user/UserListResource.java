package com.epam.esm.entity.user;


import com.epam.esm.entity.common.AbstractListResource;

import java.util.Collection;

public class UserListResource extends AbstractListResource {
    private final Collection<UserResource> users;

    public UserListResource(Collection<UserResource> users, int pageNumber, int pageSize,
                            int totalPages, long totalElements) {
        super(pageNumber, pageSize, totalPages, totalElements);
        this.users = users;
    }

    public Collection<UserResource> getUsers() {
        return users;
    }
}
