package com.epam.esm.entity.user;

import org.springframework.hateoas.RepresentationModel;

import java.util.Date;

public class UserResource extends RepresentationModel<UserResource> {
    public long id;
    public String fullName;
//    public String email;
//    public String password;
    public Date createdDate;
    public Date lastUpdatedDate;

    public UserResource(long id, String fullName, Date createdDate, Date lastUpdatedDate) {
        this.id = id;
        this.fullName = fullName;
//        this.email = email;
//        this.password = password;
        this.createdDate = createdDate;
        this.lastUpdatedDate = lastUpdatedDate;
    }
}
