package com.epam.esm.entity.giftcertificate;


public interface GiftCertificateMapper {
    GiftCertificateResourceInput asGiftCertificateResourceInput(GiftCertificate giftCertificate);
    void update(GiftCertificateResourceInput giftCertificateResource, GiftCertificate giftCertificate);
}
