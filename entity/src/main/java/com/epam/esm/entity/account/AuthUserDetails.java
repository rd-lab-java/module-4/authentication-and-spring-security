package com.epam.esm.entity.account;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
@EqualsAndHashCode(callSuper = true)
@Data
public class AuthUserDetails extends Account implements UserDetails {
    private Collection<GrantedAuthority> authorities;
    private String ip;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isAccountNonLocked();
    }

    @Override
    public boolean isAccountNonLocked() {
        return isNonLocked();
    }

    @Override
    public boolean isEnabled() {
        return isActive();
    }
}
