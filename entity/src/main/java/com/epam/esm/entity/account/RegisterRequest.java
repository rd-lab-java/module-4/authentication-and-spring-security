package com.epam.esm.entity.account;

import lombok.Data;

@Data
public class RegisterRequest {
    public final String fullName;
    public final String username;
    public final String email;
    public final String password;
}
