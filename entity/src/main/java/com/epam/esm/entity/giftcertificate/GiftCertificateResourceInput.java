package com.epam.esm.entity.giftcertificate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GiftCertificateResourceInput {
    private long id;
    private String name;
    private String description;
    private double price;
    private int duration;
}
