package com.epam.esm.entity.tag;

import org.springframework.hateoas.RepresentationModel;

public class TagResource extends RepresentationModel<TagResource> {
    public long id;
    public String name;

    public TagResource(long id, String name) {
        this.id = id;
        this.name = name;
    }
}
