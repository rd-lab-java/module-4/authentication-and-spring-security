package com.epam.esm.entity.exception;

public class IllegalOrderStatusValueException extends Exception {
    public IllegalOrderStatusValueException(String value) {
        super("Order status value is not compatible with predefined status: " + value);
    }
}
