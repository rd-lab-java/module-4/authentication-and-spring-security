package com.epam.esm.security.filter;

import com.epam.esm.entity.account.AuthUserDetails;
import com.epam.esm.util.JwtConfig;
import com.epam.esm.util.AccessAddressUtils;
import com.epam.esm.util.JWTTokenUtils;
import com.epam.esm.util.ResponseUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class AuthVerifierFilter extends OncePerRequestFilter {
    private final ObjectMapper objectMapper;
    private final Environment environment;

    public AuthVerifierFilter(ObjectMapper objectMapper, Environment environment) {
        this.objectMapper = objectMapper;
        this.environment = environment;
    }


    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request,
                                    @NonNull HttpServletResponse response,
                                    @NonNull FilterChain chain)
            throws IOException, ServletException {
        String token = request.getHeader(JwtConfig.tokenHeader);
        if(token != null && token.startsWith(JwtConfig.tokenPrefix)) {
            if(JWTTokenUtils.isBlackList(token)) {
                ResponseUtils.responseJson(response, ResponseUtils.response(
                        505, environment.getProperty("security.verifier.message.expiredAndJoinedBlackList"),
                        null), objectMapper);
                return;
            }
            // is on Redis
            if(JWTTokenUtils.hasToken(token)) {
                String ip = AccessAddressUtils.getIpAddress(request);
                String expiration = JWTTokenUtils.getExpirationByToken(token);
                String username = JWTTokenUtils.getUserNameByToken(token);

                if (JWTTokenUtils.isExpiration(expiration)) {
                    JWTTokenUtils.addBlackList(token);
                    String validTime = JWTTokenUtils.getRefreshTimeByToken(token);
                    if (JWTTokenUtils.isValid(validTime)) {
                        String newToke = JWTTokenUtils.refreshAccessToken(token);
                        JWTTokenUtils.deleteRedisToken(token);
                        JWTTokenUtils.setTokenInfo(newToke, username, ip);
                        response.setHeader(JwtConfig.tokenHeader, newToke);

                        log.info("User: {}'s Token was expired, but it is within valid refresh time, refreshed", username);
                        token = newToke;
                    } else {
                        log.info("User: {}'s Token was expired but refresh time is not valid, not refreshed", username);
                        JWTTokenUtils.addBlackList(token);
                        ResponseUtils.responseJson(response, ResponseUtils.response(
                                505, environment.getProperty("security.verifier.message.expiredAndOverdueRefreshTime"),
                                null), objectMapper);
                        return;
                    }
                }
                AuthUserDetails userDetails = JWTTokenUtils.parseAccessToken(token);

                if (userDetails != null) {
                    if (!StringUtils.equals(ip, userDetails.getIp())) {

                        log.info("User: {}'s request IP and IP on the Token is not consistent", username);

                        JWTTokenUtils.addBlackList(token);
                        ResponseUtils.responseJson(response, ResponseUtils.response(
                                505, environment.getProperty("security.verifier.message.expiredAndIpForgeryRisk"),
                                null), objectMapper);
                        return;
                    }

                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                            userDetails, userDetails.getId(), userDetails.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }
        chain.doFilter(request, response);
    }
}
