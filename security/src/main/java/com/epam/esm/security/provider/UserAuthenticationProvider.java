package com.epam.esm.security.provider;

import com.epam.esm.entity.account.AuthUserDetails;
import com.epam.esm.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    private final UserService userService;
    private final Environment environment;

    public UserAuthenticationProvider(UserService userService, Environment environment) {
        this.userService = userService;
        this.environment = environment;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();
        AuthUserDetails sysUserDetails = (AuthUserDetails) userService.loadUserByUsername(username);
        if (sysUserDetails == null) {
            throw new UsernameNotFoundException(environment.getProperty("security.authentication.message.userNotExist"));
        }

        if (!new BCryptPasswordEncoder().matches(password, sysUserDetails.getPassword())) {
            throw new BadCredentialsException(
                    environment.getProperty("security.authentication.message.usernameAndPasswordNotMatch"));
        }

        if(!sysUserDetails.isActive()) {
            throw new BadCredentialsException(environment.getProperty("security.authentication.message.notActive"));
        }

        if(!sysUserDetails.isAccountNonExpired()) {
            throw new BadCredentialsException(environment.getProperty("security.authentication.message.expired"));
        }

        if(!sysUserDetails.isAccountNonLocked()) {
            throw new BadCredentialsException(environment.getProperty("security.authentication.message.locked"));
        }

        if(!sysUserDetails.isCredentialsNonExpired()) {
            throw new BadCredentialsException(
                    environment.getProperty("security.authentication.message.credentialsExpired"));
        }

        return new UsernamePasswordAuthenticationToken(sysUserDetails, password, sysUserDetails.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
