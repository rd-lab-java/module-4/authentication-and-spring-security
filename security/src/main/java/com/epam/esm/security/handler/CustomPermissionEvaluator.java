package com.epam.esm.security.handler;

import com.epam.esm.entity.account.AuthUserDetails;
import com.epam.esm.entity.order.Order;
import com.epam.esm.service.order.OrderManager;
import com.epam.esm.service.user.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;

public class CustomPermissionEvaluator implements PermissionEvaluator {
    private final UserManager userManager;
    private final OrderManager orderManager;

    @Autowired
    public CustomPermissionEvaluator(UserManager userManager, OrderManager orderManager) {
        this.userManager = userManager;
        this.orderManager = orderManager;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        if((authentication == null) || (targetDomainObject == null) || !(permission instanceof String)) {
            return false;
        }
        String targetType = targetDomainObject.getClass().getSimpleName().toUpperCase();
        return hasPrivilege(authentication, targetType, permission.toString().toUpperCase());
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        if((authentication == null) || (targetType == null) || !(permission instanceof String)) {
            return false;
        }
        boolean accessible = hasPrivilege(authentication, targetType.toUpperCase(), permission.toString().toUpperCase());

        AuthUserDetails principal = (AuthUserDetails) authentication.getPrincipal();
        long currentUserId = userManager.getByUsername(principal.getUsername()).getId();
        if("user".equalsIgnoreCase(targetType)) {
            return accessible && currentUserId == (long) targetId;
        } else if ("order".equalsIgnoreCase(targetType)) {
            Order order = orderManager.findById((long) targetId);
            long orderOwnerId = order.getOwner().getId();
            return accessible && currentUserId == orderOwnerId;
        }
        return false;
    }

    private boolean hasPrivilege(Authentication authentication, String targetType, String permission) {
        for (GrantedAuthority grantedAuth : authentication.getAuthorities()) {
            if (grantedAuth.getAuthority().startsWith(targetType) &&
                    grantedAuth.getAuthority().contains(permission)) {
                return true;
            }
        }
        return false;
    }
}
