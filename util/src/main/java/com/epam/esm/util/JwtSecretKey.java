package com.epam.esm.util;

import io.jsonwebtoken.security.Keys;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.crypto.SecretKey;

@Configuration
@PropertySource(name = "application.properties", value = "classpath:application.properties")
public class JwtSecretKey {
    @Bean
    public SecretKey secretKey(Environment environment) {
        System.out.println("jwtConfig: " + environment.getProperty("jwt.secretKey"));
//        return Keys.hmacShaKeyFor(JwtConfig.secretKey.getBytes());
        return Keys.hmacShaKeyFor(environment.getProperty("jwt.secretKey").getBytes());
    }
}
