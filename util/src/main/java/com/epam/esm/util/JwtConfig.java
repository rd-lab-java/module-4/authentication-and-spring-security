package com.epam.esm.util;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(name = "application.properties", value = "classpath:application.properties")
@ConfigurationProperties(prefix = "jwt")
@SuppressWarnings("static-access")
public class JwtConfig {
    public static String tokenPrefix;
    public static String secretKey;
    public static Integer expiration;
    public static String antMatchers;
    public static Integer refreshTime;
    public static String tokenHeader;
    public static String loginUrl;

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    public void setTokenHeader(String tokenHeader) {
        this.tokenHeader = tokenHeader;
    }

    public void setExpiration(Integer expiration) {
    	this.expiration = expiration * 1000;
    }

    public void setRefreshTime(Integer refreshTime) {
        this.refreshTime = refreshTime * 24 * 60 * 60 * 1000;
    }
    
    public void setTokenPrefix(String tokenPrefix) {
    	this.tokenPrefix = tokenPrefix;
    }

    public void setSecretKey(String secretKey) {
        System.out.println("secret key in jwt config: " + secretKey);
    	this.secretKey = secretKey;
    }

    public void setAntMatchers(String antMatchers) {
    	this.antMatchers = antMatchers;
    }
}
