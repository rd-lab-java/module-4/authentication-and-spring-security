package com.epam.esm.writer;

import com.epam.esm.entity.tag.Tag;
import com.epam.esm.repository.tag.TagRepository;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class TagItemWriter implements ItemWriter<Tag> {
    private final TagRepository tagRepository;
    public TagItemWriter(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public void write(List<? extends Tag> items) throws Exception {
        tagRepository.saveAll(items);
    }
}
