package com.epam.esm.writer;


import com.epam.esm.entity.account.Account;
import com.epam.esm.entity.user.User;
import com.epam.esm.repository.account.AccountRepository;
import com.epam.esm.repository.user.UserRepository;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class AccountItemWriter implements ItemWriter<Account> {
    private final AccountRepository accountRepository;
    public AccountItemWriter(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void write(List<? extends Account> items) throws Exception {
        accountRepository.saveAll(items);
    }
}
