package com.epam.esm.writer;

import com.epam.esm.entity.order.Order;
import com.epam.esm.repository.order.OrderRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemWriter;

import java.util.List;

public class OrderWriter implements ItemWriter<Order> {
    private final RepositoryItemWriter<Order> itemWriter;
    public OrderWriter(OrderRepository orderRepository) {
        itemWriter = new RepositoryItemWriter<>();
        itemWriter.setRepository(orderRepository);
    }

    @Override
    public void write(List<? extends Order> items) throws Exception {
        itemWriter.write(items);
    }
}
