package com.epam.esm.writer;

import com.epam.esm.entity.giftcertificate.GiftCertificate;
import com.epam.esm.repository.giftcertificate.GiftCertificateRepository;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class GiftCertificateWriter implements ItemWriter<GiftCertificate> {
    private final GiftCertificateRepository giftCertificateRepository;
    public GiftCertificateWriter(GiftCertificateRepository giftCertificateRepository) {
        this.giftCertificateRepository = giftCertificateRepository;
    }

    @Override
    public void write(List<? extends GiftCertificate> items) throws Exception {
        giftCertificateRepository.saveAll(items);
    }
}
