package com.epam.esm.service;

public interface RandomNumberGenerator {
    int randomNumber(int min, int max);
    double randomNumber(double min, double max);
}
