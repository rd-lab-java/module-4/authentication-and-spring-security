package com.epam.esm.service.impl;


import com.epam.esm.service.RandomNumberGenerator;
import org.springframework.stereotype.Service;

@Service
public class RandomNumberGeneratorImpl implements RandomNumberGenerator {
    @Override
    public int randomNumber(int min, int max) {
        return ((int) (Math.random() * (max - min)) + min);
    }

    @Override
    public double randomNumber(double min, double max) {
        return ((Math.random() * (max - min)) + min);
    }
}
