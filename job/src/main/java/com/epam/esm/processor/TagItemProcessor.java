package com.epam.esm.processor;

import com.epam.esm.entity.tag.Tag;
import org.springframework.batch.item.ItemProcessor;

public class TagItemProcessor implements ItemProcessor<String, Tag> {
    public TagItemProcessor() {
    }

    @Override
    public Tag process(String item) throws Exception {
        Tag tag = new Tag();
        tag.setName(item);
        return tag;
    }
}
