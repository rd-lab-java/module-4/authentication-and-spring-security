package com.epam.esm.processor;


import com.epam.esm.entity.account.Account;
import com.epam.esm.entity.user.User;
import com.epam.esm.model.UserCredential;
import com.epam.esm.repository.role.RoleRepository;
import com.epam.esm.service.RandomNumberGenerator;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;
import java.util.Random;

public class AccountItemProcessor implements ItemProcessor<UserCredential, Account> {

    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final RandomNumberGenerator randomNumberGenerator;

    public AccountItemProcessor(PasswordEncoder passwordEncoder, RoleRepository roleRepository,
                                RandomNumberGenerator randomNumberGenerator) {
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.randomNumberGenerator = randomNumberGenerator;
    }

    @Override
    public Account process(UserCredential item) throws Exception {
        Account account = new Account();
        account.setUsername(item.getFirstName());
        account.setEmail(item.getEmail());
        account.setPassword(passwordEncoder.encode(
                item.getFirstName().toLowerCase() + "_" + item.getLastName().toLowerCase()));
        account.setActive(true);
        account.setAccountNonExpired(true);
        account.setCredentialsNonExpired(true);
        account.setNonLocked(true);
        User user = new User();
        user.setFullName(item.getFirstName() + " " + item.getLastName());
        account.setUser(user);
        if(randomNumberGenerator.randomNumber(1, 10) == 1) {
            account.setRoles(Collections.singleton(roleRepository.getById(2L)));
        } else {
            account.setRoles(Collections.singleton(roleRepository.getById(1L)));
        }
        return account;
    }
}
