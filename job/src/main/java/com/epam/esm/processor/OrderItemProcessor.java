package com.epam.esm.processor;


import com.epam.esm.entity.exception.IllegalOrderStatusValueException;
import com.epam.esm.entity.giftcertificate.GiftCertificate;
import com.epam.esm.entity.order.Order;
import com.epam.esm.entity.order.OrderStatus;
import com.epam.esm.entity.user.User;
import com.epam.esm.repository.user.UserRepository;
import com.epam.esm.service.RandomNumberGenerator;
import org.springframework.batch.item.ItemProcessor;

import java.util.Optional;

public class OrderItemProcessor implements ItemProcessor<GiftCertificate, Order> {
    private final UserRepository userRepository;
    private final RandomNumberGenerator numberGenerator;
    public OrderItemProcessor(UserRepository userRepository, RandomNumberGenerator numberGenerator) {
        this.userRepository = userRepository;
        this.numberGenerator = numberGenerator;
    }

    @Override
    public Order process(GiftCertificate item) throws Exception {
        Order order = new Order();
        order.setGiftCertificate(item);
        int quantity = numberGenerator.randomNumber(1, 10);
        order.setQuantity(quantity);
        order.setStatus(getStatus());
        order.setUnitPrice(item.getPrice());
        order.setTotalCost(quantity * item.getPrice());
        Optional<User> optionalUser = userRepository.findById((long) numberGenerator.randomNumber(1, 1000));
        if(optionalUser.isPresent()) {
            order.setOwner(optionalUser.get());
        } else {
            return null;
        }
//        order.setOwner(userRepository.getById((long)numberGenerator.randomNumber(1, 1000)));
        return order;
    }

    private OrderStatus getStatus() throws IllegalOrderStatusValueException {
        switch (numberGenerator.randomNumber(1, 4)) {
            case 1:
                return OrderStatus.UNPAID;
            case 2:
                return OrderStatus.PAID;
            case 3:
                return OrderStatus.CANCELLED;
            case 4:
                return OrderStatus.DONE;
            default:
                throw new IllegalOrderStatusValueException("");
        }
    }
}
