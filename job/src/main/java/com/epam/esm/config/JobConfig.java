package com.epam.esm.config;

import com.epam.esm.entity.account.Account;
import com.epam.esm.entity.giftcertificate.GiftCertificate;
import com.epam.esm.entity.order.Order;
import com.epam.esm.entity.tag.Tag;
import com.epam.esm.mapper.UserCredentialSetMapper;
import com.epam.esm.model.Country;
import com.epam.esm.model.UserCredential;
import com.epam.esm.processor.GiftCertificateProcessor;
import com.epam.esm.processor.OrderItemProcessor;
import com.epam.esm.processor.TagItemProcessor;
import com.epam.esm.processor.AccountItemProcessor;
import com.epam.esm.reader.GiftCertificateItemReader;
import com.epam.esm.repository.account.AccountRepository;
import com.epam.esm.repository.role.RoleRepository;
import com.epam.esm.service.RandomNumberGenerator;
import com.epam.esm.writer.GiftCertificateWriter;
import com.epam.esm.writer.OrderWriter;
import com.epam.esm.writer.TagItemWriter;
import com.epam.esm.writer.AccountItemWriter;
import com.epam.esm.repository.giftcertificate.GiftCertificateRepository;
import com.epam.esm.repository.order.OrderRepository;
import com.epam.esm.repository.tag.TagRepository;
import com.epam.esm.repository.user.UserRepository;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.PassThroughLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class JobConfig {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    @Qualifier(value = "transactionManager")
    private PlatformTransactionManager transactionManager;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private GiftCertificateRepository giftCertificateRepository;
    @Autowired
    private OrderRepository orderRepository;
    private static final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(10);
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private RandomNumberGenerator numberGenerator;
    private static final List<Country> countries = new ArrayList<>();

    @Bean
    public Job populateDataJob() {
        return this.jobBuilderFactory
                .get("populateDataJob")
                .start(populateTagStep())
//				.next(fetchCountriesStep())
                .next(populateAccountStep())
                .next(populateGiftCertificateStep())
                .next(populateOrderStep())
                .build();
    }

    private Step populateOrderStep() {
        return this.stepBuilderFactory.get("populateOrder")
                .<GiftCertificate, Order>chunk(10)
                .reader(giftCertificateReader())
                .processor(orderItemProcessor(userRepository, numberGenerator))
                .writer(orderWriter())
                .transactionManager(transactionManager)
                .build();
    }

    private ItemWriter<Order> orderWriter() {
        return new OrderWriter(orderRepository);
    }

    @Bean
    public ItemProcessor<GiftCertificate, Order> orderItemProcessor(UserRepository userRepository,
                                                                    RandomNumberGenerator numberGenerator) {
        return new OrderItemProcessor(userRepository, numberGenerator);
    }

    @Bean
    public ItemReader<GiftCertificate> giftCertificateReader() {
        return new GiftCertificateItemReader(giftCertificateRepository);
    }

    @Bean
    public Step populateGiftCertificateStep() {
        return this.stepBuilderFactory.get("populateGiftCertificate")
                .<String, GiftCertificate>chunk(10)
                .reader(nounReader())
                .processor(giftCertificateItemProcessor(tagRepository, numberGenerator))
                .faultTolerant()
                .skip(ConstraintViolationException.class)
                .writer(giftCertificateWriter())
                .transactionManager(transactionManager)
                .build();
    }

    @Bean
    public ItemWriter<GiftCertificate> giftCertificateWriter() {
        return new GiftCertificateWriter(giftCertificateRepository);
    }

    @Bean
    public ItemProcessor<String, GiftCertificate> giftCertificateItemProcessor(TagRepository tagRepository,
                                                                               RandomNumberGenerator numberGenerator) {
        return new GiftCertificateProcessor(tagRepository, numberGenerator);
    }

    @Bean
    public ItemReader<String> nounReader() {
        FlatFileItemReader<String> reader = new FlatFileItemReader<>();
        reader.setResource(new FileSystemResource(
                "/home/yusuf/IdeaProjects/rest-advanced-security/job/data/nouns-10000.txt"));
        reader.setLineMapper(new PassThroughLineMapper());
        return reader;
    }

    @Bean
    public Step fetchCountriesStep() {
        return this.stepBuilderFactory.get("fetchCountries").tasklet(new Tasklet() {
            @Override
            public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
                try(BufferedReader bufferedReader = new BufferedReader(new FileReader(
                        "/home/yusuf/IdeaProjects/rest-advanced-security/job/data/countries.csv"))) {
                    String line;
                    while((line = bufferedReader.readLine()) != null) {
                        String[] tokens = line.split(",");
                        countries.add(new Country(tokens[0], tokens[1]));
                    }
                    return RepeatStatus.FINISHED;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return RepeatStatus.FINISHED;
            }
        }).build();
    }

    @Bean
    public Step populateAccountStep() {
        return this.stepBuilderFactory.get("populateUserStep")
                .<UserCredential, Account>chunk(10)
//				.reader(nameReader())
                .reader(credentialReader())
                .processor(accountItemProcessor())
                .faultTolerant()
                .skip(ConstraintViolationException.class)
                .writer(accountWriter())
                .transactionManager(transactionManager)
                .build();
    }

    public static String[] tokens = new String[] {"order_id", "first_name", "last_name", "email",
            "cost", "item_id", "item_name", "ship_date"};
    @Bean
    public ItemReader<UserCredential> credentialReader() {
        FlatFileItemReader<UserCredential> reader = new FlatFileItemReader<>();
        reader.setResource(new FileSystemResource(
                "/home/yusuf/IdeaProjects/rest-advanced-security/job/data/shipped_orders.csv"));
        reader.setLinesToSkip(1);

        DefaultLineMapper<UserCredential> lineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
        tokenizer.setNames(tokens);
        lineMapper.setLineTokenizer(tokenizer);
        lineMapper.setFieldSetMapper(new UserCredentialSetMapper());
        reader.setLineMapper(lineMapper);
        return reader;
    }

    @Bean
    public ItemWriter<Account> accountWriter() {
        return new AccountItemWriter(accountRepository);
    }

    @Bean
    public ItemProcessor<UserCredential, Account> accountItemProcessor() {
        return new AccountItemProcessor(passwordEncoder, roleRepository, numberGenerator);
    }

    private ItemReader<String> nameReader() {
        FlatFileItemReader<String> itemReader = new FlatFileItemReader<>();
        itemReader.setResource(new FileSystemResource(
                "/home/yusuf/IdeaProjects/rest-advanced-security/job/data/names.txt"));
        itemReader.setLineMapper(new PassThroughLineMapper());
        return itemReader;
    }

    @Bean
    public Step populateTagStep() {
        return this.stepBuilderFactory.get("populateTagStep")
                .<String, Tag>chunk(10)
                .reader(adjectiveReader())
                .processor(tagItemProcessor())
                .faultTolerant()
                .skip(ConstraintViolationException.class)
                .writer(tagWriter())
                .transactionManager(transactionManager)
                .build();
    }

    @Bean
    public ItemWriter<Tag> tagWriter() {
        return new TagItemWriter(tagRepository);
    }

    @Bean
    public ItemProcessor<String, Tag> tagItemProcessor() {
        return new TagItemProcessor();
    }

    @Bean
    public ItemReader<String> adjectiveReader() {
        FlatFileItemReader<String> itemReader = new FlatFileItemReader<>();
        itemReader.setResource(new FileSystemResource(
                "/home/yusuf/IdeaProjects/rest-advanced-security/job/data/adjectives.txt"));
        itemReader.setLineMapper(new PassThroughLineMapper());
        return itemReader;
    }
}
