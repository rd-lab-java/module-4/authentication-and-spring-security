package com.epam.esm.reader;

import com.epam.esm.entity.giftcertificate.GiftCertificate;
import com.epam.esm.repository.giftcertificate.GiftCertificateRepository;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.data.domain.Sort;

import java.util.Collections;

public class GiftCertificateItemReader implements ItemReader<GiftCertificate> {
    private final RepositoryItemReader<GiftCertificate> itemReader;
    public GiftCertificateItemReader(GiftCertificateRepository giftCertificateRepository) {
        itemReader = new RepositoryItemReader<>();
        itemReader.setRepository(giftCertificateRepository);
        itemReader.setPageSize(10);
        itemReader.setMethodName("findAll");
        itemReader.setSort(Collections.singletonMap("id", Sort.Direction.ASC));
    }

    @Override
    public GiftCertificate read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        return itemReader.read();
    }
}
