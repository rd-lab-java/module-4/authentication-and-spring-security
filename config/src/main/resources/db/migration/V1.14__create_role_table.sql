CREATE TABLE IF NOT EXISTS role(
    id bigint NOT NULL AUTO_INCREMENT,
    role varchar(255) NOT NULL,
    PRIMARY KEY(id),
    UNIQUE KEY (role)
);