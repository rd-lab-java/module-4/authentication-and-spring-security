CREATE TABLE IF NOT EXISTS account (
    id bigint NOT NULL AUTO_INCREMENT,
    account_non_expired bit(1) not null,
    active bit(1) not null,
    credentials_non_expired bit(1) not null,
    email varchar(255) not null unique,
    non_locked bit(1) not null,
    password varchar(255) not null,
    username varchar(255) not null unique,
    user_id bigint not null,
    PRIMARY KEY(id),
    UNIQUE KEY (email, username),
    CONSTRAINT account_ibfk_1
        FOREIGN KEY (user_id)
            REFERENCES user(id)
            ON DELETE CASCADE
);