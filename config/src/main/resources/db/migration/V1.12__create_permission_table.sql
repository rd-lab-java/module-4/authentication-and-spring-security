CREATE TABLE IF NOT EXISTS permission(
    id bigint NOT NULL AUTO_INCREMENT,
    permission varchar(255) NOT NULL,
    PRIMARY KEY(id),
    UNIQUE KEY (permission)
);