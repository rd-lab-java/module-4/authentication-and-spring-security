INSERT INTO gift_certificate (id, name, description, price, duration, created_date, last_updated_date)
VALUES (1, 'Gift Certificate One', 'First Gift Certificate', 10.5, 10, '2021-08-22 16:17:17', '2021-08-22 16:17:17');

INSERT INTO gift_certificate (id, name, description, price, duration, created_date, last_updated_date)
VALUES (2, 'Gift Certificate Two', 'Second Gift Certificate', 22.5, 20, '2021-08-22 16:17:17', '2021-08-22 16:17:17');

INSERT INTO gift_certificate (id, name, description, price, duration, created_date, last_updated_date)
VALUES (3, 'Gift Certificate Three', 'Third Gift Certificate', 33.5, 30, '2021-08-22 16:17:17', '2021-08-22 16:17:17');

INSERT INTO gift_certificate (id, name, description, price, duration, created_date, last_updated_date)
VALUES (4, 'Gift Certificate Four', 'Fourth Gift Certificate', 42.5, 40, '2021-08-22 16:17:17', '2021-08-22 16:17:17');

INSERT INTO gift_certificate (id, name, description, price, duration, created_date, last_updated_date)
VALUES (5, 'Gift Certificate Five', 'Fifth Gift Certificate', 56.5, 50, '2021-08-22 16:17:17', '2021-08-22 16:17:17');

INSERT INTO gift_certificate (id, name, description, price, duration, created_date, last_updated_date)
VALUES (6, 'Gift Certificate Six', 'Sixth Gift Certificate', 64.5, 60, '2021-08-22 16:17:17', '2021-08-22 16:17:17');

INSERT INTO tag (id, name) VALUES (1, 'one');
INSERT INTO tag (id, name) VALUES (2, 'two');
INSERT INTO tag (id, name) VALUES (3, 'three');
INSERT INTO tag (id, name) VALUES (4, 'four');
INSERT INTO tag (id, name) VALUES (5, 'five');
INSERT INTO tag (id, name) VALUES (6, 'six');
INSERT INTO tag (id, name) VALUES (7, 'seven');
INSERT INTO tag (id, name) VALUES (8, 'eight');
INSERT INTO tag (id, name) VALUES (9, 'nine');
INSERT INTO tag (id, name) VALUES (10, 'ten');

INSERT INTO gift_certs_tags (gift_certificate_id, tag_id) VALUES (1, 1);
INSERT INTO gift_certs_tags (gift_certificate_id, tag_id) VALUES (1, 3);
INSERT INTO gift_certs_tags (gift_certificate_id, tag_id) VALUES (1, 5);
INSERT INTO gift_certs_tags (gift_certificate_id, tag_id) VALUES (2, 2);
INSERT INTO gift_certs_tags (gift_certificate_id, tag_id) VALUES (2, 3);
INSERT INTO gift_certs_tags (gift_certificate_id, tag_id) VALUES (2, 5);
INSERT INTO gift_certs_tags (gift_certificate_id, tag_id) VALUES (3, 4);
INSERT INTO gift_certs_tags (gift_certificate_id, tag_id) VALUES (3, 5);
INSERT INTO gift_certs_tags (gift_certificate_id, tag_id) VALUES (3, 6);
INSERT INTO gift_certs_tags (gift_certificate_id, tag_id) VALUES (4, 4);
INSERT INTO gift_certs_tags (gift_certificate_id, tag_id) VALUES (4, 5);
INSERT INTO gift_certs_tags (gift_certificate_id, tag_id) VALUES (4, 7);
INSERT INTO gift_certs_tags (gift_certificate_id, tag_id) VALUES (5, 8);
INSERT INTO gift_certs_tags (gift_certificate_id, tag_id) VALUES (5, 9);

INSERT INTO user (id, full_name, created_date, last_updated_date)
VALUES (1, 'Peter Patrick', '2020-10-09 10:48:23', '2020-10-09 10:48:23');
INSERT INTO user (id, full_name, created_date, last_updated_date)
VALUES (2, 'John Boston', '2020-10-09 10:48:23', '2020-10-09 10:48:23');
INSERT INTO user (id, full_name, created_date, last_updated_date)
VALUES (3, 'Sergey Sumaev', '2020-10-09 10:48:23', '2020-10-09 10:48:23');
INSERT INTO user (id, full_name, created_date, last_updated_date)
VALUES (4, 'Lee Hong', '2020-10-09 10:48:23', '2020-10-09 10:48:23');
INSERT INTO user (id, full_name, created_date, last_updated_date)
VALUES (5, 'Park Jung', '2020-10-09 10:48:23', '2020-10-09 10:48:23');

INSERT INTO permission(id, permission) VALUES(1, 'GIFTCERTIFICATE_READ');
INSERT INTO permission(id, permission) VALUES(2, 'GIFTCERTIFICATE_WRITE');
INSERT INTO permission(id, permission) VALUES(3, 'TAG_READ');
INSERT INTO permission(id, permission) VALUES(4, 'TAG_WRITE');
INSERT INTO permission(id, permission) VALUES(5, 'ORDER_READ');
INSERT INTO permission(id, permission) VALUES(6, 'ORDER_WRITE');
INSERT INTO permission(id, permission) VALUES(7, 'ACCOUNT_READ');
INSERT INTO permission(id, permission) VALUES(8, 'ACCOUNT_WRITE');
INSERT INTO permission(id, permission) VALUES(9, 'USER_READ');
INSERT INTO permission(id, permission) VALUES(10, 'USER_WRITE');

INSERT INTO role(id, role) VALUES(1, 'ROLE_USER');
INSERT INTO role(id, role) VALUES(2, 'ROLE_ADMIN');

INSERT INTO role_permission(role_id, permission_id) VALUES(1, 1);
INSERT INTO role_permission(role_id, permission_id) VALUES(2, 1);
INSERT INTO role_permission(role_id, permission_id) VALUES(2, 2);
INSERT INTO role_permission(role_id, permission_id) VALUES(1, 3);
INSERT INTO role_permission(role_id, permission_id) VALUES(2, 3);
INSERT INTO role_permission(role_id, permission_id) VALUES(2, 4);
INSERT INTO role_permission(role_id, permission_id) VALUES(1, 5);
INSERT INTO role_permission(role_id, permission_id) VALUES(2, 5);
INSERT INTO role_permission(role_id, permission_id) VALUES(1, 6);
INSERT INTO role_permission(role_id, permission_id) VALUES(2, 6);
INSERT INTO role_permission(role_id, permission_id) VALUES(2, 7);
INSERT INTO role_permission(role_id, permission_id) VALUES(2, 8);
INSERT INTO role_permission(role_id, permission_id) VALUES(1, 9);
INSERT INTO role_permission(role_id, permission_id) VALUES(2, 9);
INSERT INTO role_permission(role_id, permission_id) VALUES(2, 10);

INSERT INTO account (id, email, username, password, active, credentials_non_expired, non_locked,
account_non_expired, user_id)
VALUES(1, 'peter_patrick@email.com', 'peter', '$2a$10$lRWLiKrW2VzOnIH43dYTWu5ZO1BoScaT8k91oYDgo3FQXRut.e1Ui',
true, true, true, true, 1);
INSERT INTO account (id, email, username, password, active, credentials_non_expired, non_locked,
account_non_expired, user_id)
VALUES(2, 'john_boston@email.com', 'john', '$2a$10$JWGaoVsxTxTpuNkHlfTw0uya3dWRRO53J5OQGWtUSgd89bB68OV7q',
true, true, true, true, 2);
INSERT INTO account (id, email, username, password, active, credentials_non_expired, non_locked,
account_non_expired, user_id)
VALUES(3, 'sergey_sumaev@email.com', 'sergey', '$2a$10$gIDgRsU2X73SuNb/jEWHWuj8/5dA1LZahgWndN/n.LvnI3FH8IvFy',
true, true, true, true, 3);
INSERT INTO account (id, email, username, password, active, credentials_non_expired, non_locked,
account_non_expired, user_id)
VALUES(4, 'lee_hong@email.com', 'lee', '$2a$10$Jd.gfkRuAImsasBYk95t/.ErpoIMCSXYTmW/Emdq1OsnsK4SX1LAy',
true, true, true, true, 4);
INSERT INTO account (id, email, username, password, active, credentials_non_expired, non_locked,
account_non_expired, user_id)
VALUES(5, 'park_jung@email.com', 'park', '$2a$10$ePhmUUS9k1Fx.LC7clLDtOkgsQZNf0oo4V6.a/psUIhykAPLtVdCq',
true, true, true, true, 5);

INSERT INTO user_role(user_id, role_id) VALUES(1, 1);
INSERT INTO user_role(user_id, role_id) VALUES(2, 1);
INSERT INTO user_role(user_id, role_id) VALUES(3, 1);
INSERT INTO user_role(user_id, role_id) VALUES(4, 2);
INSERT INTO user_role(user_id, role_id) VALUES(5, 2);

INSERT INTO gift_certificate_order (id, quantity, status, unit_price, total_cost, owner_id, gift_certificate_id,
ordered_date, last_updated_date)
VALUES (1, 2, 'unpaid', 10.5, 21, 1, 1, '2020-10-09 10:48:23', '2020-10-09 10:48:23');
INSERT INTO gift_certificate_order (id, quantity, status, unit_price, total_cost, owner_id, gift_certificate_id,
ordered_date, last_updated_date)
VALUES (2, 3, 'paid', 10.5, 31.5, 2, 1, '2021-10-09 10:48:23', '2021-10-09 10:48:23');
INSERT INTO gift_certificate_order (id, quantity, status, unit_price, total_cost, owner_id, gift_certificate_id,
ordered_date, last_updated_date)
VALUES (3, 4, 'cancelled', 22.5, 90, 2, 2, '2020-10-09 10:48:23', '2021-10-09 10:48:23');
INSERT INTO gift_certificate_order (id, quantity, status, unit_price, total_cost, owner_id, gift_certificate_id,
ordered_date, last_updated_date)
VALUES (4, 5, 'paid', 22.5, 112.5, 1, 2, '2020-10-09 10:48:23', '2021-10-09 10:48:23');
INSERT INTO gift_certificate_order (id, quantity, status, unit_price, total_cost, owner_id, gift_certificate_id,
 ordered_date, last_updated_date)
VALUES (5, 6, 'done', 33.5, 201, 3, 3, '2020-10-09 10:48:23', '2021-10-09 10:48:23');
INSERT INTO gift_certificate_order (id, quantity, status, unit_price, total_cost, owner_id, gift_certificate_id,
ordered_date, last_updated_date)
VALUES (6, 7, 'unpaid', 56.5, 395.5, 4, 5, '2020-10-09 10:48:23', '2021-10-09 10:48:23');

TRUNCATE table gift_certificate_seq;
INSERT INTO gift_certificate_seq values(7);
TRUNCATE table tag_seq;
INSERT INTO tag_seq values(11);
TRUNCATE table user_seq;
INSERT INTO user_seq values(6);
TRUNCATE table order_seq;
INSERT INTO order_seq values(7);
TRUNCATE table account_seq;
INSERT INTO account_seq values(6);
TRUNCATE table permission_seq;
INSERT INTO permission_seq VALUES(11);
TRUNCATE table role_seq;
INSERT INTO role_seq VALUES(3);