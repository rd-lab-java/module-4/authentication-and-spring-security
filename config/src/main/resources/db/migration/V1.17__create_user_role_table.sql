CREATE TABLE IF NOT EXISTS user_role(
    user_id bigint NOT NULL,
    role_id bigint NOT NULL,
    KEY(user_id),
    KEY(role_id),
    UNIQUE KEY (user_id, role_id),
    CONSTRAINT user_role_ibfk_1
        FOREIGN KEY (user_id)
            REFERENCES account(id)
            ON DELETE CASCADE,
    CONSTRAINT user_role_ibfk_2
        FOREIGN KEY (role_id)
            REFERENCES role(id)
            ON DELETE CASCADE
);