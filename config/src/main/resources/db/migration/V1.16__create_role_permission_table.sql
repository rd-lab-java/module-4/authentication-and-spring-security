CREATE TABLE IF NOT EXISTS role_permission(
    role_id bigint,
    permission_id bigint,
    KEY(role_id),
    KEY(permission_id),
    UNIQUE KEY (role_id, permission_id),
    CONSTRAINT role_permission_ibfk_1
        FOREIGN KEY(role_id)
            REFERENCES role(id)
            ON DELETE CASCADE,
    CONSTRAINT role_permission_ibfk_2
        FOREIGN KEY(permission_id)
            REFERENCES permission(id)
            ON DELETE CASCADE
);