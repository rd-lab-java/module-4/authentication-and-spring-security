package com.epam.esm.repository;

import com.epam.esm.TestMysqlConfig;
import com.epam.esm.entity.account.Account;
import com.epam.esm.entity.user.User;
import com.epam.esm.repository.account.AccountRepository;
import com.epam.esm.repository.role.RoleRepository;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ContextConfiguration(classes = {TestMysqlConfig.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test-mysql")
@TestPropertySource(value = {"classpath:application-test.properties"})
public class AccountRepositoryTest {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    Flyway flyway;

    @BeforeEach
    public void setup() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void shouldSaveAccount() {
        Account account = new Account();
        account.setEmail("shone_tim@email.com");
        account.setUsername("Shone");
        account.setPassword("$2a$10$8BsbHu57Ki5X8gdsvEaEu.9UZn5Rg8BMKw3KCmAo5ROLzsOpOEUFi");
        account.setActive(true);
        account.setNonLocked(true);
        account.setAccountNonExpired(true);
        account.setCredentialsNonExpired(true);
        User user = new User();
        user.setFullName("Shone Tim");
        user.setCreatedDate(new Date());
        user.setLastUpdatedDate(new Date());
        account.setUser(user);
        account.setRoles(Collections.singleton(roleRepository.getById(1L)));
        Account savedAccount = accountRepository.save(account);
        assertAccount(account, savedAccount);
    }

    @Test
    public void shouldFindByUsername() {
        String username = "peter";
        Optional<Account> accountOptional = accountRepository.findByUsername(username);
        assertTrue(accountOptional.isPresent());
        assertEquals(username, accountOptional.get().getUsername());
    }

    @Test
    public void shouldReturnEmptyWhenFindByUsername() {
        String username = "pepe";
        Optional<Account> accountOptional = accountRepository.findByUsername(username);
        assertFalse(accountOptional.isPresent());
    }

    @Test
    public void shouldDeleteAccountById() {
        long userId = 1L;
        accountRepository.deleteById(userId);
        assertThrows(EmptyResultDataAccessException.class, () -> {
            accountRepository.deleteById(userId);
        });
    }

    private void assertAccount(Account expectedAccount, Account actualAccount) {
        assertEquals(expectedAccount.getId(), actualAccount.getId());
        assertEquals(expectedAccount.getEmail(), actualAccount.getEmail());
        assertEquals(expectedAccount.getUsername(), actualAccount.getUsername());
        assertEquals(expectedAccount.getPassword(), actualAccount.getPassword());
    }
}
