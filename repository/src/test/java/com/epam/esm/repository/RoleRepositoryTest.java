package com.epam.esm.repository;

import com.epam.esm.TestMysqlConfig;
import com.epam.esm.entity.account.Role;
import com.epam.esm.repository.role.RoleRepository;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ContextConfiguration(classes = {TestMysqlConfig.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test-mysql")
@TestPropertySource(value = {"classpath:application-test.properties"})
public class RoleRepositoryTest {
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private Flyway flyway;

    @BeforeEach
    public void setup() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void shouldFindByRole() {
        String role = "ROLE_USER";
        Optional<Role> roleOptional = roleRepository.findByRole(role);
        assertTrue(roleOptional.isPresent());
        assertEquals(role, roleOptional.get().getRole());
    }

    @Test
    public void shouldReturnEmptyFindByRole() {
        String role = "ROLE_MANAGER";
        Optional<Role> roleOptional = roleRepository.findByRole(role);
        assertFalse(roleOptional.isPresent());
    }
}
