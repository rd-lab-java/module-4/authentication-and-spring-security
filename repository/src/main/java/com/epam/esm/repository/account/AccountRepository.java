package com.epam.esm.repository.account;

import com.epam.esm.entity.account.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {
    @Query("select a from Account a join fetch a.user where a.username=:username")
    Optional<Account> findByUsername(@Param("username") String username);

    @Query(value = "select a from Account a join fetch a.user where a.id=:id")
    Optional<Account> findById(@Param("id") long id);
}
