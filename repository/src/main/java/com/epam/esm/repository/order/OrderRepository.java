package com.epam.esm.repository.order;

import com.epam.esm.entity.order.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    @Query("select o from Order o join fetch o.giftCertificate gc join fetch o.owner ow where o.id=:id")
    Optional<Order> findById(@Param("id") long id);
    Page<Order> findByGiftCertificateId(long giftCertificateId, Pageable pageable);
    Page<Order> findByOwnerId(long ownerId, Pageable pageable);

    @Modifying
    @Query(value = "delete from gift_certificate_order where id=:orderId and owner_id=:ownerId", nativeQuery = true)
    void deleteByIdAndOwnerId(@Param("orderId") long orderId, @Param("ownerId") long ownerId);
}
