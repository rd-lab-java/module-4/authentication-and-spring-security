package com.epam.esm.service.account;



import com.epam.esm.entity.account.Account;
import com.epam.esm.entity.account.RegisterRequest;

import java.util.Optional;

public interface AccountManager {
    Optional<Account> findByUsername(String username);
    boolean saveAccountAsUser(RegisterRequest registerRequest);
    Account deleteById(long id);
}
