package com.epam.esm.service.order;

public interface CostCalculator {
    double totalCost(int quantity, double price);
}
