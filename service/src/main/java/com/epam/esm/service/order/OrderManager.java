package com.epam.esm.service.order;

import com.epam.esm.entity.order.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrderManager {
    Order save(Order order);
    Page<Order> findByGiftCertificateId(long giftCertificateId, Pageable pageable);
    Page<Order> findByOwnerId(long ownerId, Pageable pageable);
    Order findById(long orderId);
    Order deleteById(long id);
    Order deleteByOrderIdAndOwnerId(long orderId, long ownerId);

    void deleteAll();
}
