package com.epam.esm.service.order.impl;

import com.epam.esm.service.order.CostCalculator;
import org.springframework.stereotype.Service;

@Service
public class CostCalculatorImpl implements CostCalculator {
    @Override
    public double totalCost(int quantity, double price) {
        return quantity * price;
    }
}
