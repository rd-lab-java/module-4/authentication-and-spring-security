package com.epam.esm.service.tag;

import com.epam.esm.entity.tag.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * This interface imposes CRUD operations business logic on tag object
 * @author yusuf
 *
 */
public interface TagManager {
	Tag save(Tag tag);
	Page<Tag> findByGiftCertificateId(long giftCertificateId, Pageable pageable);
	Page<Tag> findAll(Pageable pageable);
	Tag deleteById(long id);
	Tag findById(long tagId);
    Page<Tag> findMostUsedTags(Pageable pageable);
	void deleteAll();
	List<Tag> findTopMostUsedTags(int top);
}
