package com.epam.esm.service.user;

import com.epam.esm.entity.account.Account;
import com.epam.esm.entity.account.AuthUserDetails;
import com.epam.esm.entity.account.Role;
import com.epam.esm.service.account.AccountManager;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService implements UserDetailsService {
    private final AccountManager accountManager;
    private final Environment environment;

    @Autowired
    public UserService(AccountManager accountManager, Environment environment) {
        this.accountManager = accountManager;
        this.environment = environment;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Account> accountOptional = accountManager.findByUsername(username);
        Account account = accountOptional.<UsernameNotFoundException>orElseThrow(() -> {
            throw new UsernameNotFoundException(
                    String.format(environment.getProperty("security.service.message.notFound"), username));
        });
        return buildAccountForAuthentication(account, getAuthority(account.getRoles()));
    }

    private List<GrantedAuthority> getAuthority(Set<Role> roles) {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for(Role role: roles) {
            grantedAuthorities.addAll(role.getGrantedAuthorities());
        }
        return grantedAuthorities;
    }

    private UserDetails buildAccountForAuthentication(Account account, List<GrantedAuthority> authorities) {
        AuthUserDetails userDetails = new AuthUserDetails();
        BeanUtils.copyProperties(account, userDetails);
        userDetails.setAuthorities(authorities);
        return userDetails;
    }
}
