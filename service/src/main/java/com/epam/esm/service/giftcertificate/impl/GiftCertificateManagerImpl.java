package com.epam.esm.service.giftcertificate.impl;


import com.epam.esm.entity.giftcertificate.GiftCertificate;
import com.epam.esm.entity.tag.Tag;
import com.epam.esm.repository.giftcertificate.GiftCertificateRepository;
import com.epam.esm.repository.tag.TagRepository;
import com.epam.esm.service.exception.GiftCertificateDuplicateException;
import com.epam.esm.service.exception.GiftCertificateNotFoundException;
import com.epam.esm.service.giftcertificate.GiftCertificateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

/**
 * Gift Certificate Interface Implementations
 * that contains implementation of business logics. 
 * @author yusuf
 *
 */
@Service
@PropertySource(name="stringValues", value = "classpath:string-values.properties")
public class GiftCertificateManagerImpl implements GiftCertificateManager {
	private final GiftCertificateRepository giftCertificateRepository;
	private final TagRepository tagRepository;
	private final Environment environment;

	@Autowired
	public GiftCertificateManagerImpl(GiftCertificateRepository giftCertificateRepository, TagRepository tagRepository, Environment environment) {
		this.giftCertificateRepository = giftCertificateRepository;
		this.tagRepository = tagRepository;
		this.environment = environment;
	}

	@Override
	@Transactional
	public GiftCertificate save(GiftCertificate giftCertificate) {
		try {
			giftCertificate.getTags().stream()
					.filter(tag -> {
						Optional<Tag> tagOptional = tagRepository.findByName(tag.getName());
						tagOptional.ifPresent(foundTag -> tag.setId(foundTag.getId()));
						return !tagOptional.isPresent();
					})
					.forEach(tagRepository::save);

			return giftCertificateRepository.saveAndFlush(giftCertificate);
		} catch (DataIntegrityViolationException ex) {
			throw new GiftCertificateDuplicateException(
					String.format(environment.getProperty("giftCertificate.message.duplicate"),
							giftCertificate.getName()), ex);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Page<GiftCertificate> findAll(Pageable pageable) {
		return giftCertificateRepository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public GiftCertificate findById(long id) {
		return giftCertificateRepository.findById(id).<GiftCertificateNotFoundException>orElseThrow(() -> {
			throw new GiftCertificateNotFoundException(
					String.format(environment.getProperty("giftCertificate.message.notFound"), id));
		});
	}

	@Override
	@Transactional
	public GiftCertificate update(GiftCertificate giftCertificate) {
		giftCertificate.setUpdatedDate(new Date());
		giftCertificateRepository.updateById(giftCertificate.getId(), giftCertificate.getName(),
				giftCertificate.getDescription(), giftCertificate.getPrice(), giftCertificate.getDuration(),
				giftCertificate.getUpdatedDate());

		giftCertificate.getTags().
				forEach(tag -> {
					Optional<Tag> tagOptional = tagRepository.findByName(tag.getName());
					tagOptional.ifPresent(foundTag -> tag.setId(foundTag.getId()));
					if(!tagOptional.isPresent()) {
						tagRepository.save(tag);
					}
					giftCertificateRepository.saveGiftCertificateAndTag(giftCertificate.getId(), tag.getId());
				});
		return giftCertificate;
	}

	@Override
	@Transactional
	public GiftCertificate deleteById(long id) {
	    Optional<GiftCertificate> giftCertificateOptional = giftCertificateRepository.findById(id);
	    GiftCertificate giftCertificate = giftCertificateOptional.<GiftCertificateNotFoundException>orElseThrow(() -> {
			throw new GiftCertificateNotFoundException(
	        		String.format(environment.getProperty("giftCertificate.message.notFound"), id));
        });
	    giftCertificateRepository.delete(giftCertificate);
	    return giftCertificate;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<GiftCertificate> findByTagNames(Optional<String[]> tagNames, Pageable pageable) {
		if(tagNames.isPresent()) {
			return giftCertificateRepository.findByTagNames(tagNames.get(), tagNames.get().length, pageable);
		}
		throw new GiftCertificateNotFoundException(
				environment.getProperty("giftCertificate.message.searchNotFoundByTag"));
	}

	@Override
	@Transactional(readOnly = true)
	public Page<GiftCertificate> findByNameContaining(Optional<String> partOfName, Pageable pageable) {
		if(partOfName.isPresent()) {
			return giftCertificateRepository.findByNameContaining(partOfName.get(), pageable);
		}
		throw new GiftCertificateNotFoundException(
				environment.getProperty("giftCertificate.message.searchNotFoundByName"));
	}


	@Override
	@Transactional(readOnly = true)
	public Page<GiftCertificate> findByDescriptionContaining(Optional<String> partOfDesc, Pageable pageable) {
		if(partOfDesc.isPresent()) {
			return giftCertificateRepository.findByDescriptionContaining(partOfDesc.get(), pageable);
		}
		throw new GiftCertificateNotFoundException(
				environment.getProperty("giftCertificate.message.searchNotFoundByDescription"));
	}

	@Override
	@Transactional(readOnly = true)
	public Page<GiftCertificate> findByTagId(long tagId, Pageable pageable) {
		return giftCertificateRepository.findByTagId(tagId, pageable);
	}


	@Override
	@Transactional
	public void deleteAll() {
		giftCertificateRepository.deleteAll();
	}

}
