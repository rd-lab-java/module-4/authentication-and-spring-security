package com.epam.esm.service.account.impl;


import com.epam.esm.entity.account.Account;
import com.epam.esm.entity.account.RegisterRequest;
import com.epam.esm.entity.user.User;
import com.epam.esm.repository.account.AccountRepository;
import com.epam.esm.repository.role.RoleRepository;
import com.epam.esm.service.account.AccountManager;
import com.epam.esm.service.exception.UserDuplicateException;
import com.epam.esm.service.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.Optional;

@Service
public class AccountManagerImpl implements AccountManager {
    private final Environment environment;
    private final AccountRepository accountRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public AccountManagerImpl(Environment environment, AccountRepository accountRepository, RoleRepository roleRepository) {
        this.environment = environment;
        this.accountRepository = accountRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = new BCryptPasswordEncoder(10);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Account> findByUsername(String username) {
        return accountRepository.findByUsername(username);
    }


    @Override
    @Transactional
    public boolean saveAccountAsUser(RegisterRequest registerRequest) {
        try {
            Account account = new Account();
            account.setUsername(registerRequest.username);
            account.setEmail(registerRequest.email);
            account.setPassword(passwordEncoder.encode(registerRequest.password));
            account.setActive(true);
            account.setNonLocked(true);
            account.setAccountNonExpired(true);
            account.setCredentialsNonExpired(true);
            User user = new User();
            user.setCreatedDate(new Date());
            user.setLastUpdatedDate(new Date());
            user.setFullName(registerRequest.fullName);
            account.setUser(user);
            roleRepository.findByRole("ROLE_USER").ifPresent(role -> account.setRoles(Collections.singleton(role)));
            accountRepository.saveAndFlush(account); // used saveAndFlush, since exception is thrown
                                                     // after transaction commits
        } catch (DataIntegrityViolationException ex) {
            throw new UserDuplicateException(String.format(environment.getProperty("account.message.error"),
                    ex.getMostSpecificCause()));
        }
        return true;
    }

    @Override
    @Transactional
    public Account deleteById(long id) {
        Account account = accountRepository.findById(id).<UserNotFoundException>orElseThrow(() -> {
            throw new UserNotFoundException(String.format(environment.getProperty("account.message.notFound"), id));
        });
        accountRepository.deleteById(id);
        return account;
    }
}
