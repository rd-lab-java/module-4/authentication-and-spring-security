package com.epam.esm.service.user;


import com.epam.esm.entity.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface UserManager {
    User save(User user);
    Page<User> findAll(Pageable pageable);
    User findById(long id);
    User deleteById(long userId);
    User getByUsername(String username);

    void deleteAll();
}
