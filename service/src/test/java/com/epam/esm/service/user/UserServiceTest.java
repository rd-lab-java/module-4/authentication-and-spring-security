package com.epam.esm.service.user;

import com.epam.esm.TestMysqlConfig;
import com.epam.esm.repository.account.AccountRepository;
import com.epam.esm.repository.role.RoleRepository;
import com.epam.esm.service.account.AccountManager;
import com.epam.esm.service.account.impl.AccountManagerImpl;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ContextConfiguration(classes = {TestMysqlConfig.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test-mysql")
@TestPropertySource(value = {"classpath:application-test.properties"})
@PropertySource(name = "stringValues", value = "classpath:string-values.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserServiceTest {
    UserService userService;
    AccountManager accountManager;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    Environment environment;
    @Autowired
    Flyway flyway;

    @BeforeAll
    public void setup() {
        accountManager = new AccountManagerImpl(environment, accountRepository, roleRepository);
        userService = new UserService(accountManager, environment);
    }

    @BeforeEach
    public void init() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    void shouldLoadUserByUsername() {
        String username = "peter";
        UserDetails userDetails = userService.loadUserByUsername(username);
        assertEquals(username, userDetails.getUsername());
    }

    @Test
    void shouldThrowExceptionWhenLoadUserByUsername() {
        String username = "koushik";
        assertThrows(UsernameNotFoundException.class, ()-> {
            userService.loadUserByUsername(username);
        });
    }
}