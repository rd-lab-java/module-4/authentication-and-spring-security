package com.epam.esm.service;


import com.epam.esm.TestMysqlConfig;
import com.epam.esm.entity.giftcertificate.GiftCertificate;
import com.epam.esm.entity.order.Order;
import com.epam.esm.entity.order.OrderStatus;
import com.epam.esm.entity.user.User;
import com.epam.esm.repository.giftcertificate.GiftCertificateRepository;
import com.epam.esm.repository.order.OrderRepository;
import com.epam.esm.repository.tag.TagRepository;
import com.epam.esm.repository.user.UserRepository;
import com.epam.esm.service.exception.OrderNotFoundException;
import com.epam.esm.service.giftcertificate.GiftCertificateManager;
import com.epam.esm.service.giftcertificate.impl.GiftCertificateManagerImpl;
import com.epam.esm.service.order.CostCalculator;
import com.epam.esm.service.order.OrderManager;
import com.epam.esm.service.order.impl.OrderManagerImpl;
import com.epam.esm.service.user.UserManager;
import com.epam.esm.service.user.impl.UserManagerImpl;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestMysqlConfig.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test-mysql")
@TestPropertySource(value = {"classpath:application-test.properties"})
@PropertySource(name = "stringValues", value = "classpath:string-values.properties")
class OrderManagerTest {
    private final static int DEFAULT_PAGE_NUMBER = 0;
    private final static int DEFAULT_PAGE_SIZE = 3;
    @Autowired
    Environment environment;
    @Autowired
    OrderRepository orderRepository;
    OrderManager orderManager;
    @Autowired
    GiftCertificateRepository giftCertificateRepository;
    GiftCertificateManager giftCertificateManager;
    @Autowired
    UserRepository userRepository;
    UserManager userManager;
    @Autowired
    TagRepository tagRepository;
    @Autowired
    EntityManager entityManager;
    @Mock
    CostCalculator costCalculator;
    @Autowired
    Flyway flyway;

    @BeforeEach
    public void init() {
        giftCertificateManager = new GiftCertificateManagerImpl(giftCertificateRepository, tagRepository, environment);
        userManager = new UserManagerImpl(userRepository, environment);
        orderManager = new OrderManagerImpl(orderRepository, costCalculator, environment);
        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void shouldSaveOrder() {
        long giftCertificateId = 3L;
        GiftCertificate giftCertificate = giftCertificateManager.findById(giftCertificateId);
        long userId = 1L;
        User user = userManager.findById(userId);

        int quantity = 10;
        when(costCalculator.totalCost(quantity, giftCertificate.getPrice())).
                thenReturn(quantity * giftCertificate.getPrice());
        Order order = new Order(quantity, OrderStatus.UNPAID, user, giftCertificate);
        entityManager.joinTransaction();
        Order saveOrder = orderManager.save(order);

        assertOrder(order, saveOrder);
        verify(costCalculator, VerificationModeFactory.times(1)).
                totalCost(quantity, giftCertificate.getPrice());
    }

    @Test
    public void shouldFindOrderById() {
        long orderId = 3L;
        Order order = orderManager.findById(orderId);
        assertEquals(orderId, order.getId());
    }

    @Test
    public void shouldThrowExceptionWhenFindOrderById() {
        long orderId = 7L;
        assertThrows(OrderNotFoundException.class, () -> {
            orderManager.findById(orderId);
        });
    }

    @Test
    public void findOrdersByGiftCertificateId() {
        long giftCertificateId = 1L;

        Page<Order> orderPage = orderManager.findByGiftCertificateId(
                giftCertificateId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertEquals(2, orderPage.getTotalElements());
        assertEquals(0, orderPage.getNumber());
        assertEquals(1, orderPage.getTotalPages());
        assertEquals(2, orderPage.getNumberOfElements());

        assertTrue(orderPage.hasContent());
        assertFalse(orderPage.hasNext());
        assertFalse(orderPage.hasPrevious());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificateId() {
        long giftCertificateId = 4L;
        Page<Order> orderPage = orderManager.findByGiftCertificateId(
                giftCertificateId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, orderPage.getTotalElements());
    }

    @Test
    public void shouldFindOrdersByOwnerId() {
        long userId = 2L;
        Page<Order> orderPage = orderManager.findByOwnerId(
                userId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertEquals(2, orderPage.getTotalElements());
        assertEquals(0, orderPage.getNumber());
        assertEquals(1, orderPage.getTotalPages());
        assertEquals(2, orderPage.getNumberOfElements());

        assertTrue(orderPage.hasContent());
        assertFalse(orderPage.hasNext());
        assertFalse(orderPage.hasPrevious());
    }

    @Test
    public void shouldReturnEmptyWhenFindOwnerId() {
        long ownerId = 5L;
        Page<Order> orderPage = orderManager.findByOwnerId(
                ownerId, PageRequest.of(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(0, orderPage.getTotalElements());
    }

    @Test
    public void shouldDeleteOrder() {
        long orderId = 1L;
        Order order = orderManager.deleteById(orderId);
        assertEquals(orderId, order.getId());
    }

    @Test
    @DisplayName("throw exception when delete order")
    public void shouldThrowExceptionWhenDeleteOrder() {
        long orderId = 7L;
        assertThrows(OrderNotFoundException.class, () -> {
            orderManager.deleteById(orderId);
        });
    }

    @Test
    public void shouldDeleteOrderByOrderIdAndOwnerId() {
        long orderId = 1L;
        long ownerId = 1L;
        entityManager.joinTransaction();
        Order order = orderManager.deleteByOrderIdAndOwnerId(orderId, ownerId);
        assertEquals(orderId, order.getId());
        assertEquals(ownerId, order.getOwner().getId());
    }

    @DisplayName("test for checking non-existed resource with order id")
    @Test
    public void shouldReturnExceptionWhenDeleteOrderByOrderIdAndOwnerIdA() {
        long orderId = 10L;
        long ownerId = 1L;
        entityManager.joinTransaction();
        assertThrows(OrderNotFoundException.class, () -> {
            orderManager.deleteByOrderIdAndOwnerId(orderId, ownerId);
        });
    }

    @DisplayName("test for checking non-existed resource with order id and owner id")
    @Test
    public void shouldReturnExceptionWhenDeleteOrderByOrderIdAndOwnerIdB() {
        long orderId = 4L;
        long ownerId = 2L;
        entityManager.joinTransaction();
        assertThrows(OrderNotFoundException.class, () -> {
            orderManager.deleteByOrderIdAndOwnerId(orderId, ownerId);
        });
    }

    private void assertOrder(Order expectedOrder, Order actualOrder) {
        assertEquals(expectedOrder.getId(), actualOrder.getId());
        assertEquals(expectedOrder.getQuantity(), actualOrder.getQuantity());
        assertEquals(expectedOrder.getStatus(), actualOrder.getStatus());
        assertEquals(expectedOrder.getTotalCost(), actualOrder.getTotalCost());
        assertEquals(expectedOrder.getOwner().getId(), actualOrder.getOwner().getId());
        assertEquals(expectedOrder.getGiftCertificate().getId(), actualOrder.getGiftCertificate().getId());
        assertEquals(expectedOrder.getOrderedDate(), actualOrder.getOrderedDate());
    }
}