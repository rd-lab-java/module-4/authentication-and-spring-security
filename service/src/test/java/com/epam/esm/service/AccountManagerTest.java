package com.epam.esm.service;

import com.epam.esm.TestMysqlConfig;
import com.epam.esm.entity.account.Account;
import com.epam.esm.entity.account.RegisterRequest;
import com.epam.esm.repository.account.AccountRepository;
import com.epam.esm.repository.role.RoleRepository;
import com.epam.esm.service.account.AccountManager;
import com.epam.esm.service.account.impl.AccountManagerImpl;
import com.epam.esm.service.exception.UserDuplicateException;
import com.epam.esm.service.exception.UserNotFoundException;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import javax.persistence.EntityManager;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ContextConfiguration(classes = {TestMysqlConfig.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test-mysql")
@TestPropertySource(value = {"classpath:application-test.properties"})
@PropertySource(name = "stringValues", value = "classpath:string-values.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AccountManagerTest {
    AccountManager accountManager;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    Flyway flyway;
    @Autowired
    Environment environment;
    @Autowired
    EntityManager entityManager;

    @BeforeAll
    public void setup() {
        accountManager = new AccountManagerImpl(environment, accountRepository, roleRepository);
    }

    @BeforeEach
    public void init() {

        flyway.clean();
        flyway.migrate();
    }

    @Test
    public void shouldSaveAccountAsUser() {
        RegisterRequest registerRequest = new RegisterRequest(
                "Anton Pavlov1", "Anton1", "anton_pavlov1@email.com", "anton_pavlov");
        entityManager.joinTransaction();
        boolean isSaved = accountManager.saveAccountAsUser(registerRequest);
        assertTrue(isSaved);
    }

    @Test
    public void shouldReturnFalseWhenSaveAccountAsUser() {
        RegisterRequest registerRequest = new RegisterRequest(
                "Peter Patrick", "peter", "peter_patrick@email.com",
                "$2a$10$lRWLiKrW2VzOnIH43dYTWu5ZO1BoScaT8k91oYDgo3FQXRut.e1Ui");
        entityManager.joinTransaction();
        assertThrows(UserDuplicateException.class, () -> {
            accountManager.saveAccountAsUser(registerRequest);
        });
    }

    @Test
    public void shouldFindByUsername() {
        String username = "peter";
        Optional<Account> accountOptional = accountManager.findByUsername(username);
        assertTrue(accountOptional.isPresent());
        assertEquals(username, accountOptional.get().getUsername());
    }

    @Test
    public void shouldFindEmptyWhenFindByUsername() {
        String username = "Kozim";
        Optional<Account> accountOptional = accountManager.findByUsername(username);
        assertFalse(accountOptional.isPresent());
    }

    @Test
    public void shouldDeleteById() {
        long id = 1L;
        Account account = accountManager.deleteById(id);
        assertEquals(id, account.getId());
    }

    @Test
    public void shouldReturnExceptionWhenDeleteById() {
        long id = 20000L;
        assertThrows(UserNotFoundException.class, () -> {
            accountManager.deleteById(id);
        });
    }
}
